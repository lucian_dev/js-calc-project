define(function(){
	/**
		* Model Calc - can compute and show the result
		* @param {int} output - get value
		* @param {int} count - update if the output result is change
	*/
	function Calc(){
		this.output = 0;
		this.count = 0;

		/**
		* Set result and compute; increment the flag "count"
		* @param  {string} string - string expression
		*/
		this.setResult = function(string){
			this.output = eval(string);
			if(this.output == undefined) this.output = 0;
			this.count++;
		};

		/**
		* Check is change the count var
		* @return  {bool} true/false
		*/
		this.isChange = function(){
           if(this.count > 0) return true;
           return false;
		}

		/**
		* Return the result
		* @return  {int} output
		*/
		this.getResult = function(){
			this.count = 0;
			return this.output;
		};
	}

	return Calc;
});