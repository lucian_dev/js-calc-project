define(function(){

    function render(param){
        var appDiv = document.getElementById('app');
        var html = '<div class="display"><input type="text" readonly id="decimal" class="decimal" value="'+ param.result+'" size="18" ></div><div class="keys" id="keys">'+
                '<input type="button" class="button black" id="i"  data-calc="<" value="&larr;">' +
                '<input type="button" class="button black"   data-calc="C" value="C">' +
                '<input type="button" class="button black"   data-calc="sqr" value="&radic;">' +
                '<input type="button" class="button black"   data-calc="per" value="&#37;">' +
                '<input type="button" class="button black" data-calc="7" value="7">' +
                '<input type="button" class="button black" data-calc="8" value="8">' +
                '<input type="button" class="button black" data-calc="9" value="9">' +
                '<input type="button" class="button pink"    data-calc="/" value="&divide;">' +
                '<input type="button" class="button black" data-calc="4" value="4">' +
                '<input type="button" class="button black" data-calc="5" value="5">' +
                '<input type="button" class="button black" data-calc="6" value="6">' +
                '<input type="button" class="button pink"  data-calc="*" value="&times;">' +
                '<input type="button" class="button black" data-calc="1" value="1">' +
                '<input type="button" class="button black" data-calc="2" value="2">' +
                '<input type="button" class="button black" data-calc="3" value="3">' +
                '<input type="button" class="button pink"  data-calc="-" value="-">' +
                '<input type="button" class="button orange"  data-calc="=" value="=">' +
                '<input type="button" class="button black"   data-calc="0" value="0">' +
                '<input type="button" class="button black"   data-calc="." value=".">' +
                '<input type="button" class="button pink"  data-calc="+" value="+">' +
            '</p>'+
        '</div>';

        appDiv.innerHTML = html;
    }

    return {
        render:render
    };
});