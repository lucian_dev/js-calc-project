//put all the event logic in the controller
define(['../models/CalcModel', '../views/CalcView','../plugin/PubSub'], function(Calc, View, Observer){

    var myCalc = new Calc();
    var output = '';

	/** Create a view with Calc and add listeners for click event*/
    function start(){
        //output
        View.render({result:0});

        output = document.getElementById('decimal');
        //listeners form view
        bindEvents();
    }

    /** Bind to the click event */
    function bindEvents(){
        var buttons = getAllElementsFromParrent('keys');
        if(buttons){
            for (var i = 0; i < buttons.length; i++) {
                var button = buttons[i];
                button.addEventListener('click', eventList, false);
            }
        }
    }

	/**
		* Check if update de textbox or update the MODEL calc.
		* @return null
	*/
    function eventList(){
            console.log('click Event: ' + this.value);
            var res = this.value;
            var attr = this.getAttribute("data-calc");

            //declare subscriber
            Observer.subscribe("update/calc", observerCalcChange);
            Observer.subscribe("update/textbox", observerInputText);

            if(hasAttribute(attr)){
                Observer.publish("update/textbox", {result: attr, msg: "Update textbox"});
                return;
            }
            //allow numbers, point, signs
            if(isNumber(res) || isSign(res)){
                Observer.publish("update/textbox", {result: res, msg: "Update textbox"});

            } else if(isEqual(res)){
                Observer.publish("update/calc", {sign: res, msg: "Update calculator"});

            } else {
                alert('not implemented');
            }
    }

    //utils
	/**
		* Validate is Number.
		* @param  {string} x - value from button.
		* @return {bool}  
	*/
    function isNumber(x){
        var reg = /^\d+$/;
        if(reg.test(x)) return true;
        return false;
    }
	/**
		* Validate is Sign.
		* @param  {string} x - value from button.
		* @return {bool}  
	*/
    function isSign(x){
        var reg = /^[\*\-\+\/\.]$/;
        if(reg.test(x)) return true;
        return false;
    }
	/**
		* Validate is custom attribute.
		* @param  {string} x - value from button.
		* @return {bool}  
	*/
    function hasAttribute(x){
        var reg = /^[\*\/C<]$/;
        if(reg.test(x)) return true;
        return false;
    }
	/**
		* Validate is push Equal.
		* @param  {string} x - value from button.
		* @return {bool}  
	*/
    function isEqual(x){
        var reg = /^[=]$/;
        if(reg.test(x)) return true;
        return false;
    }
	
	
	/**
		* Return children for binding click event
		* @param  {string} x - value from button.
		* @return {mixture} null/ array obj - return list with object children 
	*/
    function getAllElementsFromParrent(idName){
       var divElement = document.getElementById(idName);
       if(divElement == null) return null;
       return  divElement.childNodes;
    }

    /** business logic */
	/**
		* Change the Input Text Value.
		* @param {object} event.result and event.msg - get value and show a status message.
	*/
    function observerInputText(event){
        // console.log(event.result);

        //reset value
        if(event.result == 'C') {
            output.value = 0;
            //notify the Calc model
        } else {
            var tmpVal = output.value.toString();

            var lastChar = tmpVal.slice(-1);

            if(event.result == '<') {tmpVal = tmpVal.substring(0, tmpVal.length-1); event.result= '';}
            //check last char if is a sign
            if(isSign(lastChar) && isSign(event.result)) { tmpVal = tmpVal.substring(0, tmpVal.length-1);}          
			//check if is not 0
            if(tmpVal == 0 && !isSign(event.result) && tmpVal.length == 1) {tmpVal = ''; }
			
            output.value =  (tmpVal + event.result).toString().trim();
        }

        console.log(event.msg);
    }

	/**
		* Check id Model Calc value is change and update the Input Text.
		* @param {object} event.msg - show a status message.
	*/
    function observerCalc(event){

        console.log(event.msg);
        if(myCalc.isChange()) {
          output.value = myCalc.getResult(); console.log("Res Calc is change");
        }
    }

	/**
		* Compute Model Calc and update the input text 
		* @param {object} event.msg - show a status message.
	*/
    function observerCalcChange(event){
        Observer.subscribe("check/calc", observerCalc);
        //update calc model
        console.log(event.msg);
        myCalc.setResult(output.value);

        Observer.publish("check/calc", {msg:"Check the Model Calc is change"});
    }

    return {
        start:start
    };
});