// Revealing Module Pattern
define(function(){
	var subscribers = {};

	function subscribe(type, fn){
		if(!subscribers[type]){
			subscribers[type] = [];
		}

		if(subscribers[type].indexOf(fn) == -1){
			subscribers[type].push(fn);
		}
	}

	function unsubscribe(type, fn){
		var listeners = subscribers[type];

		if(!listeners){
			return;
		}

		var index = listeners.indexOf(fn);
		if(index > -1) {
			listeners.splice(index, 1);
		}
	}

	function publish(type, eventObj){
		if(!subscribers[type]){
			return;
		}

		if(!eventObj.type) {
			eventObj.type = type;
		}

		var listeners = subscribers[type];

		for (var i=0, max = listeners.length; i< max; i++){
			listeners[i](eventObj);
		}
	}

	return {
		subscribe: subscribe,
		unsubscribe: unsubscribe,
		publish: publish
	};
});
