Create HTML5 calculator page with button 0 to 9 that can perform following tasks: addition, substraction, and multiplication. Code have implement following patterns:
 
- module pattern
- MVC pattern
- publisher-subscriber pattern in an explicit way (attaching handlers to events will not be counted as pub-sub pattern implementation)
- please do not use any external JavaScript frameworks
- (optional) requireJS - JavaScript file and module loader